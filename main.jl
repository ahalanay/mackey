using Plots
using DelayDiffEq

β(θ,f,s,x)=f*(θ^s)/(θ^s+x^s)

function hemo_model(du,u,h,pars,t)
    θ,f,s,κ,τ,γ=pars
    A=2*exp(-γ*τ)
    hist=h(pars,t-τ)[1]
    du[1]=-(κ+β(θ,f,s,u[1]))*u[1]+A*β(θ,f,s,u[1])*hist
end

τ=2.8
lags = [τ]
A=2*exp(-γ*τ)
u_echil=θ*(f*(A-1)/κ-1)^(1/s)
u0=[u_echil]
γ=0.1
f=8.0
s=2
θ=0.08
κ=0.02
h(pars,t) = u0
pars = [θ,f,s,κ,τ,γ]
tspan=(0.0,10000.0)
prob=DDEProblem(hemo_model, u0, h, tspan, pars; constant_lags=lags)
alg=MethodOfSteps(Tsit5())
sol = solve(prob,alg)
pyplot()
plot(sol,size=(1500,1500))
